<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/webhook', function (Request $request) {
    $bot = new \Telegram\Bot\Api(config('telegram.bots.common.token'));
    $bot->addCommands(config('telegram.bots.common.commands'));
    $bot->commandsHandler(true);

    $update = $bot->getWebhookUpdate();
    $type = $update->detectType();

    if ($update->isType('callback_query')) {
        $callbackData = $update->getCallbackQuery()->getData();
        $bot->answerCallbackQuery([
            'callback_query_id' => $update->getCallbackQuery()->getId(),
            'text' => "Тип события: $type.\ncallback_data: $callbackData",
            'show_alert' => true
        ]);
    }

    $replyMarkupKeyboard = \Telegram\Bot\Keyboard\Keyboard::make([
        'keyboard' => [
            [ '/call_inline_keyboard_markup' ]
        ],
        'resize_keyboard' => true,
        'one_time_keyboard' => true
    ]);

    $bot->sendMessage([
        'chat_id' => $update->getChat()->getId(),
        'text' => "Тип события: $type.\n```\n" . json_encode($update, JSON_PRETTY_PRINT) . "\n```",
        'parse_mode' => 'markdown',
        'reply_markup' => $replyMarkupKeyboard
    ]);
});

Route::get('/webhook/enable', function () {
    $bot = new \Telegram\Bot\Api(config('telegram.bots.common.token'));
    return response()->json($bot->setWebhook([
        'url' =>'https://r-yukhno.me/api/webhook'
    ])->getDecodedBody());
});

Route::get('/webhook/disable', function () {
    $bot = new \Telegram\Bot\Api(config('telegram.bots.common.token'));
    return response()->json($bot->removeWebhook()->getDecodedBody());
});

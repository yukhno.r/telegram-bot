<?php

namespace App\Telegram\Commands;

use Telegram\Bot\Commands\Command;

class StartCommand extends Command
{
    protected $name = 'start';

    /**
     * @inheritDoc
     */
    public function handle($arguments)
    {
        $name = $this->getUpdate()->getChat()->getFirstName()
            . ' '
            . $this->getUpdate()->getChat()->getLastName();

        $this->replyWithMessage([
            'text' => "Привет, $name. Я буду присылать тебе все, что получаю в объекте Update от тебя."
        ]);
    }
}
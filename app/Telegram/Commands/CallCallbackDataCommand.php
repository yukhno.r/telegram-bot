<?php

namespace App\Telegram\Commands;

use Telegram\Bot\Commands\Command;
use Telegram\Bot\Keyboard\Keyboard;

class CallCallbackDataCommand extends Command
{
    protected $name = 'call_inline_keyboard_markup';

    protected $description = 'Отправляет InlineKeyboard';

    /**
     * @inheritDoc
     */
    public function handle($arguments)
    {
        $keyboard = Keyboard::make([
            'inline_keyboard' => [
                [ ['text' => 'first row', 'callback_data' => 'this_is_callback_data_from first row'] ],
                [ ['text' => 'second row', 'callback_data' => 'this_is_callback_data_from SECOND row'], ['text' => 'second row, 2 column', 'callback_data' => 'this_is_callback_data_from 2 row, 2 col'] ],
            ]
        ]);

        $this->replyWithMessage([
            'text' => 'This is InlineKeyboardMarkup',
            'reply_markup' => $keyboard
        ]);
    }
}